#!/bin/bash
podman run -dit \
  --name roundcube \
  --pod mailserver-pod \
  -e ROUNDCUBEMAIL_DEFAULT_HOST=${HOSTNAME} \
  -e ROUNDCUBEMAIL_DEFAULT_PORT=10143 \
  -e ROUNDCUBEMAIL_SMTP_SERVER=${HOSTNAME} \
  -e ROUNDCUBEMAIL_SMTP_PORT=10587 \
  -e ROUNDCUBEMAIL_DB_TYPE=sqlite \
  -v /var/roundcube/db:/var/roundcube/db:Z \
  docker.io/roundcube/roundcubemail
