#!/bin/bash
podman pod create \
  --name mailserver-pod \
  --hostname "${HOSTNAME}" \
  --network mailserver-net \
  --publish 10110:110 \
  --publish 10465:465 \
  --publish 10995:995 \
  --publish 1025:25 \
  --publish 10143:143 \
  --publish 10587:587 \
  --publish 10993:993 \
  --publish 8080:80
