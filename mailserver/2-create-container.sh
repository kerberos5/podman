#!/bin/bash
podman run -dit \
  --name mailserver \
  --pod mailserver-pod \
  -e ENABLE_SPAMASSASSIN=1 \
  -e SPAMASSASSIN_SPAM_TO_INBOX=1 \
  -e ENABLE_CLAMAV=1 \
  -e ENABLE_POSTGREY=1 \
  -e ENABLE_FAIL2BAN=0 \
  -e ENABLE_SASLAUTHD=0 \
  -e ONE_DIR=1 \
  -e TZ=Europe/Rome \
  -v /var/mailserver/data:/var/mail:Z \
  -v /var/mailserver/state:/var/mail-state:Z \
  -v /var/mailserver/logs:/var/log/mail:Z \
  -v /var/mailserver/config:/tmp/docker-mailserver:Z \
  docker.io/mailserver/docker-mailserver
