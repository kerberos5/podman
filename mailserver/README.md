# Mailserver deploy
Deploy containerized mailserver and web interface using roundcube. The deployments are both root less

## References
* https://github.com/docker-mailserver/docker-mailserver
* https://hub.docker.com/r/roundcube/roundcubemail/

## Deployment sequence
* Follow the deployment sequence

``` bash
# Environment preparation phase
0-prepare.sh

# Create pod
1-create-pod.sh

# Deploy mailserver container
2-create-container.sh

# Configure mailserver container
3-config-mailserver.sh

# OR manual configuration
change **dovecot.conf** and **main.cf**

podman exec -it mailserver bash -c 'nano /etc/dovecot/dovecot.conf'
# enable: listen = *

podman exec -it mailserver bash -c 'nano /etc/postfix/main.cf'
# change: inet_protocols = ipv4

# Create first user (change your custom doamin)
4-create-user.sh

# Deploy roundcube container web interface
5-roundcube.sh

# View containers info
podman ps --format "{{.ID}} {{.Names}} {{.Status}}"
```

* Create service using systemd persistent to reboot

``` bash
loginctl enable-linger
loginctl show-user $(whoami)

cd  ~/.config/systemd/user/

podman generate systemd --name mailserver --files --new
podman generate systemd --name roundcube --files --new

podman stop mailserver
podman rm mailserver

podman stop roundcube
podman rm roundcube

systemctl --user daemon-reload
systemctl --user enable --now container-mailserver.service
systemctl --user enable --now container-roundcube.service

# Manage container whit
systemctl --user start|stop|restart|status container-<container_name>.service
```

* Roundcube Webmail console url

``` bash
http://<HOSTNAME>:8080
```
