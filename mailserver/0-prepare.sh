#!/bin/bash
# Create directory
sudo mkdir /var/mailserver/{data,state,logs,config} -p
sudo mkdir -p /var/roundcube/db
sudo chown -R $(whoami): /var/mailserver
sudo chown -R $(whoami): /var/roundcube

# Create network
podman network create mailserver-net
