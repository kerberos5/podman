#!/bin/bash
# Create first user
podman run --rm \
  --pod mailserver-pod \
  -e MAIL_USER=devops@istruzione.it \
  -e MAIL_PASS=supersecret \
  -it mailserver/docker-mailserver /bin/sh \
  -c 'echo "$MAIL_USER|$(doveadm pw -s SHA512-CRYPT -u $MAIL_USER -p $MAIL_PASS)"' >> /var/mailserver/config/postfix-accounts.cf
