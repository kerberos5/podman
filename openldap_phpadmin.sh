#!/bin/bash
### RIF: https://computingforgeeks.com/run-openldap-server-in-docker-containers/?amp

## fase preliminare 
sudo mkdir -p /srv/slapd/database
sudo mkdir /srv/slapd/config
sudo chown -R devops: /srv/slapd

## inserire la risoluzione dell'hostname del servizio in /etc/hosts
sudo echo "172.16.180.177 ldap-dev.sidi.mpi.it" >> /etc/hosts

## creo il pod che ospiterà i contenitori
podman pod create --name openldap --hostname ldap-dev.sidi.mpi.it -p 10389:389 -p 10636:636 -p 10080:80 -p 10443:443

## creo il container openldap-server
podman run \
--name openldap-server \
--pod openldap \
-e LDAP_ORGANISATION=example \
-e LDAP_DOMAIN=example.com \
-e LDAP_ADMIN_USERNAME=admin \
-e LDAP_ADMIN_PASSWORD=admin_pass \
-e LDAP_CONFIG_PASSWORD=config_pass \
-e "LDAP_BASE_DN=dc=example,dc=com" \
-e LDAP_READONLY_USER=true \
-e LDAP_READONLY_USER_USERNAME=user-ro \
-e LDAP_READONLY_USER_PASSWORD=ro_pass \
-e LDAP_TLS=true \
-v /srv/slapd/database:/var/lib/ldap:Z \
-v /srv/slapd/config:/etc/ldap/slapd.d:Z \
-d docker.io/osixia/openldap:latest

## creo il contaier phpldapadmin
podman run \
--name phpldapadmin \
--pod openldap \
-e PHPLDAPADMIN_LDAP_HOSTS=openldap-server \
-e PHPLDAPADMIN_HTTPS=true \
-d docker.io/osixia/phpldapadmin:latest

## Se voglio montare anche i certificati custom nel container openldap-server
sudo mkdir /srv/certificates
sudo chown -R devops: /srv/certificates

## inserire le seguenti direttive
-e LDAP_TLS_CA_CRT_FILENAME=<DOMAIN>.ca.crt \
-e LDAP_TLS_CRT_FILENAME=server.crt \
-e LDAP_TLS_KEY_FILENAME=server.key \
