# usage
podman build -t docker.io/kerberos5/nginx:1.0 .
podman push docker.io/kerberos5/nginx:1.0
podman run -d --rm --name nginx -p 8080:80 docker.io/kerberos5/nginx:1.0