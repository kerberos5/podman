#!/bin/sh

# Sostituisce le variabili d'ambiente nel file index.html
envsubst < /var/www/html/index.html > /var/www/html/index.html.tmp && mv /var/www/html/index.html.tmp /var/www/html/index.html

# Esegue Apache in foreground
exec httpd -D FOREGROUND
