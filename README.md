# Podman resources and examples
## Practical exercises to prepare for EX188

### Install podman and skopeo
``` bash
$ sudo dnf install podman -y
$ sudo dnf install skopeo -y
```
### Install container-tools
``` bash
$ sudo yum module install container-tools -y
$ sudo cp Zscaler* /etc/pki/ca-trust/source/anchors/
$ sudo update-ca-trust extract
```

### Install podman-compose
``` bash
$ sudo dnf install pip -y
$ pip3 install podman-compose
```