### usage
# Podman
podman build -t docker.io/kerberos5/httpd:scc .
podman push qdocker.io/kerberos5/httpd:scc
podman run -d --rm --name httpd -p 8888:80 docker.io/kerberos5/httpd:scc

# Openshift
oc create sa httpd-scc-sa
oc adm policy add-scc-to-user anyuid -z httpd-scc-sa
oc new-app --name httpd-scc --image quay.io/kerberos5/httpd:scc
oc set sa deployment/httpd-scc httpd-scc-sa
